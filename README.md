# Rodinia benchmark UMA version

To run, first generate required data file
```bash
./uma/datagen.py
```

Then run with the bash script
```bash
./run.sh
```

After the first time you generate the data files, you do not need to generate 
them again.


