#!/usr/bin/python
import subprocess
import os

root_path = os.getcwd()
nvprof_path = r'/usr/local/cuda/bin/nvprof'
workloads = [
    ('gaussian', r'./gaussian -s 512 -q', ''),
    ('nn', r'./nn filelist_4 -r 10 -lat 90 -lng 30 -q'),
    ('hotspot', r'./hotspot 512 2 20 ../../data/hotspot/temp_512 ../../data/hotspot/power_512 output.out')
]


def main():
    for workload in workloads:
        run_workloads(workload)

def run_workloads(setting):
    print "Running benchmark", setting[0]
    cwd = root_path + '/cuda/' + setting[0]
    p = subprocess.Popen(r'make clean && make NON_UNIFIED=1', cwd=cwd, shell=True)
    p.wait()
    p = subprocess.Popen(nvprof_path + ' ' + setting[1], cwd=cwd, shell=True)
    p.wait()

    p = subprocess.Popen(r'make clean && make UNIFIED=1', cwd=cwd, shell=True)
    p.wait()
    p = subprocess.Popen(nvprof_path + ' ' + setting[1], cwd=cwd, shell=True)
    p.wait()

    p = subprocess.Popen(r'make clean && make UNIFIED=1 OPTIMIZED=1', cwd=cwd, shell=True)
    p.wait()
    p = subprocess.Popen(nvprof_path + ' ' + setting[1], cwd=cwd, shell=True)
    p.wait()

if __name__ == "__main__":
    main()
