#!/usr/bin/python
import os
import subprocess

root_path = os.getcwd()
data_gen = [
    (root_path + r'/data/nn/inputGen', r'make && ./gen_dataset.sh')
]

def main():
    for gen in data_gen:
        p = subprocess.Popen(gen[1], cwd=gen[0], shell=True)

if __name__ == "__main__":
    main()
